<?php
require_once 'controlador/tombolaControl.php';
$instancia = ControlTombola::singleton_tombola();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Cambios</title>
	<link rel="stylesheet" href="css/cambio.css">
	<script src="js/jquery.js"></script>
	<script src="js/cambio.js"></script>
</head>
<body>
	<div id="tabla">
		<table border="1" cellpadding="10" cellspacing="0" bordercolor="#666666" style="border-collapse:collapse;">
			<thead>
				<tr>
					<th>Numeros Ganadores</th>
					<th>Premio</th>
					<th>Estado</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="body">
				<?php
				$ganadores = $instancia->cambioControl();
				$i         = 0;
				foreach ($ganadores as $num) {
					$numero = $num['numero'];
					$premio = $num['premio'];
					$estado = $num['visto'];
					$i++;
					if ($numero != 'Sigue Intentando') {
						if ($estado == 'si') {
							?>
							<tr>
								<td><?php echo $numero; ?></td>
								<td><?php echo $premio; ?></td>
								<td>Inhabilitado</td>
								<td>
									<center>

										<input type="button" class="boton" value="Activar" id="<?php echo $numero; ?>">
									</center>
								</td>
								<!--<td>
									<center>
										<input type="button" class="boton2" value="Ocultar" id="<?php echo $numero; ?>">
									</center>
								</td>-->
							</tr>
							<?php
						} else {
							?>
							<tr>
								<td><?php echo $numero; ?></td>
								<td><?php echo $premio; ?></td>
								<td>Inhabilitado</td>
								<!--<td>
									<center>

										<input type="button" class="boton" value="Activar" id="<?php echo $numero; ?>">
									</center>
								</td>-->
								<td>
									<center>
										<input type="button" class="boton2" value="Ocultar" id="<?php echo $numero; ?>">
									</center>
								</td>
							</tr>
							<?php
						}
					}
				}
				?>
			</tbody>
		</table>
	</div>
</body>
</html>