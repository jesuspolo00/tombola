// Script to randomise the panel the tombola lands on

$('.tombola').mouseenter(function() {
    var rotation = [1440, 1485, 1530, 1575, 1620, 1665, 1710, 1755];
    var pick = Math.floor(Math.random() * 8);
    var spin = rotation[pick];
    $('.tombola').css({ 'transform': 'rotateX(' + spin + 'deg) translateZ(-480px)' });
});

$('.tombola').mouseleave(function() {
    $('.tombola').css({ 'transform': 'rotateX(0deg) translateZ(-480px)' });
});