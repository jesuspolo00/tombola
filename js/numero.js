$(document).ready(function() {
	$(".push--flat").focus();
	var audio = document.getElementById("audio");
	$(".push--flat").click(function() {
		$(".push--flat").attr('disabled', true);
		setTimeout(validarNumero, 2100);
		$(".number_1").attr("class", "number_1 animacion_1");
		$(".number_2").attr("class", "number_2 animacion_2");
		$(".number_3").attr("class", "number_3 animacion_3");
		$(".number_4").attr("class", "number_4 animacion_4");
	});

	function validarNumero() {
		$("#fondo").hide();
		var usuario = 0;
		var postData = {
			"existe": usuario
		};
		var dataString = JSON.stringify(postData);
		try {
			$.ajax({
				url: 'validar.php',
				data: {
					numExi: dataString
				},
				type: 'POST',
				dataType: "json",
				success: function(resultado) {
					if (resultado != "") {
						var visto = resultado['numero'];
						var string = visto.length;
						$("#val").html(resultado['numero']);
						if (visto == 'Sigue Intentando') {
							var id = resultado['id'];
							$(".number_1").attr("class", "number_1");
							$(".number_2").attr("class", "number_2");
							$(".number_3").attr("class", "number_3");
							$(".number_4").attr("class", "number_4");
							Swal.fire({
								position: 'center',
								title: visto,
								type: 'error',
								showConfirmButton: false,
								timer: 1500
							}).then((willDelete) => {
								if (willDelete) {
                                    //window.location.replace('index.php');
                                    guardarsigue(id);
                                    //setTimeout(actualizar,1600);
                                }
                            });
						} else if (visto != 0) {
							audio.play();
							setTimeout(pausar_1, 400);
							setTimeout(pausar_2, 300);
							setTimeout(pausar_3, 200);
							setTimeout(pausar_4, 100);
							if (string == 1) {
                                //alert(visto);
                                var numero_1 = visto.substr(0, 1);
                                $(".number_4").val(numero_1);
                                $(".number_3").val('0');
                                $(".number_2").val('0');
                                $(".number_1").val('0');
                                guardar(visto);
                                changeImage();
                                //setInterval(changeImage, 0);
                                setTimeout(actualizar, 2700);
                            } else if (string == 2) {
                            	var numero_1 = visto.substr(0, 1);
                            	var numero_2 = visto.substr(1, 1);
                            	$(".number_4").val(numero_2);
                            	$(".number_3").val(numero_1);
                            	$(".number_2").val('0');
                            	$(".number_1").val('0');
                            	guardar(visto);
                            	changeImage();
                                //setInterval(changeImage, 0);
                                setTimeout(actualizar, 2700);
                            } else if (string == 3) {
                            	var numero_1 = visto.substr(0, 1);
                            	var numero_2 = visto.substr(1, 1);
                            	var numero_3 = visto.substr(2, 1);
                            	$(".number_4").val(numero_3);
                            	$(".number_3").val(numero_2);
                            	$(".number_2").val(numero_1);
                            	$(".number_1").val('0');
                            	guardar(visto);
                            	changeImage();
                                //setInterval(changeImage, 0);
                                setTimeout(actualizar, 2700);
                            } else if (string == 4) {
                            	var numero_1 = visto.substr(0, 1);
                            	var numero_2 = visto.substr(1, 1);
                            	var numero_3 = visto.substr(2, 1);
                            	var numero_4 = visto.substr(3, 1);
                            	$(".number_4").val(numero_4);
                            	$(".number_3").val(numero_3);
                            	$(".number_2").val(numero_2);
                            	$(".number_1").val(numero_1);
                            	guardar(visto);
                            	changeImage();
                                //setInterval(changeImage, 0);
                                setTimeout(actualizar, 2700);
                            }
                            Swal.fire({
                            	position: 'bottom-end',
                            	title: visto,
                            	imageUrl: 'img/winner.svg',
                            	imageWidth: 100,
                            	imageHeight: 200,
                            	imageAlt: 'Custom image',
                            	animation: false,
                            	showConfirmButton: false,
                            	timer: 2500
                            }).then((willDelete) => {
                            	if (willDelete) {
                                    //guardar(visto);
                                }
                            });
                        } else {}
                    } else {
                    	Swal.fire({
                    		position: 'center',
                    		title: 'Numeros Agotados',
                    		type: 'error',
                    		showConfirmButton: false,
                    		timer: 1500
                    	}).then((willDelete) => {
                    		if (willDelete) {
                    			window.location.replace('index.php');
                                //guardarsigue(id);
                            }
                        });
                    }
                }
            });
} catch (evt) {
	alert(evt.message);
}
}

function guardar(nume) {
	try {
		$.ajax({
			url: 'guardar.php',
			method: 'POST',
			data: {
				'numero': nume
			},
			cache: false,
			success: function(resultado) {
				if (resultado = 'ok') {
					$("#body").reload();
                        //window.location.replace('index.php');
                        //$("#aprendiz_det" + id).css('background', ' #27ae60 ');
                    } else {}
                }
            });
	} catch (evt) {
		alert(evt.message);
	}
}

function guardarsigue(id) {
	try {
		$.ajax({
			url: 'guardar_sigue.php',
			method: 'POST',
			data: {
				'numero': id
			},
			cache: false,
			success: function(resultado) {
				if (resultado = 'ok') {
                        //$("#body").reload();
                        window.location.replace('index.php');
                        //$("#aprendiz_det" + id).css('background', ' #27ae60 ');
                    } else {}
                }
            });
	} catch (evt) {
		alert(evt.message);
	}
}

function actualizar() {
	window.location.replace("index.php");
}

function pausar_1() {
	$(".number_1").attr("class", "number_1");
}

function pausar_2() {
	$(".number_2").attr("class", "number_2");
}

function pausar_3() {
	$(".number_3").attr("class", "number_3");
}

function pausar_4() {
	$(".number_4").attr("class", "number_4");
}

function changeImage() {
	$("#body").css("background-image", "url(img/fondo2.gif)");
}
});