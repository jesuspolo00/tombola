<?php

require_once 'conexion.php';

class ModeloTombola extends conexion
{

    public function guardarNumerosModel($datos)
    {
        $tabla  = 'numeros';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = 'INSERT INTO ' . $tabla . ' (numero,visto) VALUES (:a,:g)';
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':a', $datos['numero']);
            $preparado->bindParam(':g', $datos['visto']);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                //echo "<script>alert('No se ha guardado el administrador')</script>";
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public function validarNumeroModel($dato)
    {
        //$tabla = 'numeros';
        $enlace = conexion::singleton_conexion();
        $query  = "SELECT * FROM numeros where visto = 'no' order by rand() limit 1";
        try {
            $preparado = $enlace->preparar($query);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $enlace->closed();
        $enlace = null;
    }

    public function GuardarNumeroModel($dato)
    {
        $tabla  = 'numeros';
        $enlace = conexion::singleton_conexion();
        $query  = "UPDATE " . $tabla . " SET visto  = 'si', hora = :h WHERE numero = :id";
        try {
            $preparado = $enlace->preparar($query);
            $preparado->bindValue(':id', $dato['num']);
            $preparado->bindParam(':h', $dato['hora']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $enlace->closed();
        $enlace = null;
    }

    public function GuardarSigueModel($dato)
    {
        $tabla  = 'numeros';
        $enlace = conexion::singleton_conexion();
        $query  = "UPDATE " . $tabla . " SET visto  = 'si' WHERE id = :id";
        try {
            $preparado = $enlace->preparar($query);
            $preparado->bindValue(':id', $dato['num']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $enlace->closed();
        $enlace = null;
    }

    public function activarModel($dato)
    {
        $tabla  = 'numeros';
        $enlace = conexion::singleton_conexion();
        $query  = "UPDATE " . $tabla . " SET visto  = 'no' WHERE numero = " . $dato . "";
        try {
            $preparado = $enlace->preparar($query);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $enlace->closed();
        $enlace = null;
    }

    public function ocultarModel($dato)
    {
        $tabla  = 'numeros';
        $enlace = conexion::singleton_conexion();
        $query  = "UPDATE " . $tabla . " SET visto  = 'si' WHERE numero = " . $dato . "";
        try {
            $preparado = $enlace->preparar($query);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $enlace->closed();
        $enlace = null;
    }

    public function verGanadoresModel()
    {
        $tabla  = 'numeros';
        $enlace = conexion::singleton_conexion();
        $query  = "SELECT * FROM numeros WHERE visto = 'si' order by hora desc";
        try {
            $preparado = $enlace->preparar($query);
            //$preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $enlace->closed();
        $enlace = null;
    }

    public function cambioModel()
    {
        $tabla  = 'numeros';
        $enlace = conexion::singleton_conexion();
        $query  = "SELECT * FROM numeros WHERE tipo = 'g' and hora = '00:00:00'";
        try {
            $preparado = $enlace->preparar($query);
            //$preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $enlace->closed();
        $enlace = null;
    }

}
