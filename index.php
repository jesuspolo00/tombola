<?php
require_once 'controlador/tombolaControl.php';
$instancia = ControlTombola::singleton_tombola();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Tombola</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/numero.js"></script>
	<script type="text/javascript" src="js/sweet_alert.js"></script>
	<link rel="stylesheet" type="text/css" href="css/1.css">
	<link rel="stylesheet" type="text/css" href="css/push_button.css">
</head>
<body id="body">
	<!--<div id="fondo"></div>-->
	<div id="diseno">
		<div id="contenido">
			<div class="vista_1">
				<input type="text" class="number_1" disabled value="0">
			</div>
			<div class="vista_2">
				<input type="text" class="number_2" disabled value="0">
			</div>
			<div class="vista_3">
				<input type="text" class="number_3" disabled value="0">
			</div>
			<div class="vista_4">
				<input type="text" class="number_4" disabled value="0">
			</div>
		</div>
	</div>


	<!-- <img src="img/logo.png" class="img_logo"> -->
	<!--<div id="ganadores">
		<div id="numeros">
			<table>
				<thead>
					<tr>
						<th>Numeros Ganadores</th>
						<th>Premio</th>
					</tr>
				</thead>
				<tbody id="body">
					<?php
$ganadores = $instancia->verGanadoresControl();
$i         = 0;
foreach ($ganadores as $num) {
    $id     = $num['id'];
    $numero = $num['numero'];
    $premio = $num['premio'];
    $i++;
    if ($numero != 'Sigue Intentando') {
        ?>
							<tr>
								<td><?php echo $numero; ?></td>
								<td><?php echo $premio; ?></td>
							</tr>
							<?php
}
}
?>
				</tbody>
			</table>
		</div>
	</div>-->
	<div class="input-boton">
		<button class="push--flat"></button>
	</div>
	<!--<a href="lista.php">
		<input type="button" value="Descargar Lista" class="list">
	</a>-->
	<audio id="audio" controls>
		<source type="audio/wav" src="img/sonido.mp3">
		</audio>
	</body>
	</html>