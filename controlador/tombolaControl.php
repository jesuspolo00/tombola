<?php
date_default_timezone_set('America/Bogota');
require_once 'modelo/tombolaModel.php';

class ControlTombola
{

    private static $instancia;

    public static function singleton_tombola()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function guardarNumeroControl()
    {
        print_r($_POST);
        if ($_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['numero']) &&
            !empty($_POST['numero'])
        ) {
            $numero = $_POST['numero'];
        for ($i = 0; $i <= $numero; $i++) {

            $valores = array(
                'numero' => $i,
                'visto'  => 'no',
            );
            $guardar = ModeloTombola::guardarNumerosModel($valores);
        }
    } else {

    }
}

public function verGanadoresControl()
{
    $ganador = ModeloTombola::verGanadoresModel();
    return $ganador;
}

public function cambioControl()
{
    $ganador = ModeloTombola::cambioModel();
    return $ganador;
}

public function validarNumeroControl()
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST' &&
        isset($_POST['numExi']) &&
        !empty($_POST['numExi'])
    ) {

        $verificar_num = json_decode(stripslashes($_POST['numExi']));
    $verUs         = ModeloTombola::validarNumeroModel($verificar_num->existe);
    return $verUs;
}
}

public function VistoNumeroControl()
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST' &&
        isset($_POST['numero']) &&
        !empty($_POST['numero'])) {

        $hora  = date('h:i:s');
    $valor = array(
        'num'  => $_POST['numero'],
        'hora' => $hora,
    );
    $result = ModeloTombola::GuardarNumeroModel($valor);

    if ($result) {

        $r = "ok";
    } else {
        $r = "No";
    }
    return $r;
}
}

public function VistoSigueControl()
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST' &&
        isset($_POST['numero']) &&
        !empty($_POST['numero'])) {

        $id     = filter_input(INPUT_POST, 'numero', FILTER_SANITIZE_NUMBER_INT);
    $valor  = array('num' => $_POST['numero']);
    $result = ModeloTombola::GuardarSigueModel($valor);

    if ($result) {

        $r = "ok";
    } else {
        $r = "No";
    }
    return $r;
}
}

public function activarControl()
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST' &&
        isset($_POST['numero']) &&
        !empty($_POST['numero'])) {

        $id     = filter_input(INPUT_POST, 'numero', FILTER_SANITIZE_NUMBER_INT);
    $valor  = $_POST['numero'];
    $result = ModeloTombola::activarModel($valor);

    if ($result) {

        $r = "ok";
    } else {
        $r = "No";
    }
    return $r;
}
}

public function ocultarControl()
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST' &&
        isset($_POST['numero']) &&
        !empty($_POST['numero'])) {

        $id     = filter_input(INPUT_POST, 'numero', FILTER_SANITIZE_NUMBER_INT);
    $valor  = $_POST['numero'];
    $result = ModeloTombola::ocultarModel($valor);

    if ($result) {

        $r = "ok";
    } else {
        $r = "No";
    }
    return $r;
}
}

}
