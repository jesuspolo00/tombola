<?php
header("Pragma: public");
header("Expires: 0");
$filename = "Ganadores.xls";
header("Content-type: application/x-msdownload");
header("Content-Disposition: attachment; filename=$filename");
header("Pragma: no-cache");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
require_once 'controlador/tombolaControl.php';
$instancia = ControlTombola::singleton_tombola();
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<table border="1" cellpadding="10" cellspacing="0" bordercolor="#666666" style="border-collapse:collapse;">
		<thead>
			<tr>
				<th>Numeros Ganadores</th>
				<th>Premio</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$ganadores = $instancia->verGanadoresControl();
			$i         = 0;
			foreach ($ganadores as $num) {
				$numero = $num['numero'];
				$premio = $num['premio'];
				$i++;
				if ($numero != 'Sigue Intentando') {
					?>
					<tr>
						<td><?php echo $numero; ?></td>
						<td><?php echo $premio; ?></td>
					</tr>
					<?php
				}
			}
			?>
		</tbody>
	</table>
</body>
</html>