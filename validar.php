<?php
date_default_timezone_set('America/Bogota');
require_once 'controlador/tombolaControl.php';

$instAsistencia = ControlTombola::singleton_tombola();
$asistencia     = $instAsistencia->validarNumeroControl();
echo json_encode($asistencia);
